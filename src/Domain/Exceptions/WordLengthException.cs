﻿namespace Domain.Exceptions;
public class WordLengthException : Exception
{
    public WordLengthException(int min, int max) : base($"Word length should be equal or larger than {min} and smaller than or equal to {max}")
    {
    }
}
