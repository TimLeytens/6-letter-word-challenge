﻿using Application;
using Application.Common;
using Application.Repositories;
using Application.Services;
using Infrastructure.Common;
using Infrastructure.Repositories;
using Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ConsoleApp;
internal static class ConfigureServices
{
    internal static IServiceCollection AddApplicationServices(this IServiceCollection services)
    {
        services.AddTransient<WordChallenge>();
        services.AddTransient<IWordRepository, WordRepository>();
        services.AddTransient<IWordService, WordService>();
        services.AddTransient<IFileReader, TextFileReader>();
        services.AddTransient<ICombinationProvider, TwoWordsCombinationProvider>();
        return services;
    }
}
