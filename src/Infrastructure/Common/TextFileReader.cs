﻿using Application.Common;

namespace Infrastructure.Common;
public class TextFileReader : IFileReader
{

    public IEnumerable<string> Read(string filename)
    {
        string[] lines = new string[0];

        try
        {
            lines = System.IO.File.ReadAllLines(filename);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        return new List<string>(lines);
    }
}
